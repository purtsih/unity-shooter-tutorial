﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed = 6f;

    Vector3   movement; // Movement to apply
    Animator  anim;     // Animator
    Rigidbody rb;       // Player rigidbody

    int floorMask; // Raycast mask
    float camRayLength = 100f;

    // Set up references
    void Awake()
    {
        floorMask = LayerMask.GetMask("Floor");
        anim = GetComponent<Animator>();
        rb   = GetComponent<Rigidbody>();
    }

    // Do physics update
    void FixedUpdate()
    {
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");

        // Apply movement
        Move(h, v);
        Turn();
        Animating(h, v);
    }


    // Move the player
    void Move(float h, float v)
    {
        movement.Set(h, 0f, v); // Don't move in Y-axis
        // Normalize
        movement = movement.normalized * speed * Time.deltaTime;

        // Apply movement
        rb.MovePosition(transform.position + movement);
    }


    void Turn()
    {
        // Cast a ray from the camera to the floor quad
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(camRay, out hit, camRayLength, floorMask))
        {
            Vector3 playerToMouse = hit.point - transform.position;
            playerToMouse.y = 0f;

            Quaternion newRotation = Quaternion.LookRotation(playerToMouse);
            rb.MoveRotation(newRotation);
        }
    }


    void Animating(float h, float v)
    {
        bool walking = (h != 0 || v != 0);
        anim.SetBool("IsWalking", walking);
    }
}
